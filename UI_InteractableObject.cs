﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Image))]
public class UI_InteractableObject : MonoBehaviour, IGameObjectPooled
{
    #region Variables
    [Header("UI Components")]
    [SerializeField] private Image m_bar;
    [SerializeField] private TextMeshProUGUI m_text;

    [Space]
    [Header("Animator")]
    [SerializeField] private Animator m_animator;
    public enum ActivationType
    {
        Partial,
        Complete
    }

    #region Private
    private Image m_minimalUI;
    private GameObjectPool m_pool;
    #endregion

    #endregion

    #region Built In Methods
    private void Awake()
    {
        m_minimalUI = GetComponent<Image>();
    }

    private void Start()
    {
        m_bar.enabled = false;
        m_text.enabled = false;
    }
    #endregion

    #region Custom Methods

    #region Properties

    public GameObjectPool Pool { 
        get => m_pool;
        set
        {
            if (m_pool == null)
                m_pool = value;
            else
                throw new System.Exception("Bad pool use, this should only get set once!");
        } 
    }

    #endregion

    #region Public Methods
    public void ActiveUI(ActivationType _activationType)
    {
        switch (_activationType)
        {
            case ActivationType.Partial:
                PartialOpen();
                break;
            case ActivationType.Complete:
                CompleteOpen();
                break;
            default:
                break;
        }
    }

    public void DisableUI(ActivationType _activationType)
    {
        switch (_activationType)
        {
            case ActivationType.Partial:
                PartialClose();
                break;
            case ActivationType.Complete:
                CompleteClose();
                break;
            default:
                break;
        }
    }

    public void SetTextFrom(InteractableBase _interactableObject)
    {
        m_text.text = _interactableObject.ActionName;
    }
    #endregion

    #region Private Methods
    private void PartialOpen()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);

        m_minimalUI.enabled = true;
    }

    private void CompleteOpen()
    {
        m_bar.enabled = true;
        m_text.enabled = true;
        PartialOpen();
    }

    private void PartialClose()
    {
        m_minimalUI.enabled = false;
    }

    private void CompleteClose()
    {
        m_bar.enabled = false;
        m_text.enabled = false;
        PartialClose();
    }
    #endregion

    #endregion

}

internal interface IGameObjectPooled
{
    GameObjectPool Pool { get; set; }
}