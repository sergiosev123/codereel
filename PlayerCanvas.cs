﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class PlayerCanvas : Singleton<PlayerCanvas>
{
    #region Data
    [Header("Data")]
    [SerializeField] private InteractionData interactionData;
    #endregion

    #region Variables
    [Space]
    [Header("UI Prefab")]
    public GameObject barPrefab;

    [Space]
    [Header("Settings")]
    [SerializeField] private Transform container;
    [SerializeField] private Vector3 positionOffset = new Vector3(0.0f, 0.5f, 0.0f);

    [Space]
    [SerializeField] private GameObject inGameplayGroup;

    [Space]
    [SerializeField] private Image ballImage;
    [SerializeField] private Color colorOnInteraction;

    #region Private
    private List<RegisteredInteractableObject> m_interactableUIObject = new List<RegisteredInteractableObject>();
    #endregion
    #endregion

    #region Built In Methods

    private void Update()
    {
        RefreshInteractableObjectUIPos();

        SetBallColor();
    }
    #endregion

    #region Custom Methods

    #region Properties
    public Transform Container
    {
        get => container;
    }
    #endregion

    #region Interactable Objects UI
    void RefreshInteractableObjectUIPos()
    {
        foreach(RegisteredInteractableObject _regIO in m_interactableUIObject)
        {
            if (_regIO.InteractableObject == null)
            {
                _regIO.InteractableObjectUI.DisableUI(UI_InteractableObject.ActivationType.Complete);
                GameObjectPool.Instance.SaveInteractableObjectUI(_regIO.InteractableObjectUI.gameObject);
                m_interactableUIObject.Remove(_regIO);
                return;
            }

            _regIO.InteractableObjectUI.gameObject.SetActive(_regIO.InteractableObject.IsInteractable);

            Vector3 _position = Camera.main.WorldToScreenPoint(_regIO.InteractableObject.transform.position);

            if (Camera.main.WorldToScreenPoint(_regIO.InteractableObject.transform.position).z > 0.0f)
                _regIO.InteractableObjectUI.transform.position = _position + positionOffset;
        }
    }

    public void RegisterInteractableObject(InteractableBase _interactableObject)
    {
        RegisteredInteractableObject _regIO = new RegisteredInteractableObject(_interactableObject);

        m_interactableUIObject.Add(_regIO);
        _regIO.InteractableObjectUI.SetTextFrom(_interactableObject);
        _regIO.InteractableObjectUI.ActiveUI(UI_InteractableObject.ActivationType.Partial);
    }

    public void IO_SetUIVisible(InteractableBase _interactableObject, bool _value)
    {
        RegisteredInteractableObject _regIO = m_interactableUIObject.Find(x => x.InteractableObject == _interactableObject);

        if (_regIO == null)
        {
            RegisterInteractableObject(_interactableObject);
            return;
        }

        if (_value)
        {

            _regIO.InteractableObjectUI.ActiveUI(UI_InteractableObject.ActivationType.Partial);
        } 
        else
        {
            _regIO.InteractableObjectUI.DisableUI(UI_InteractableObject.ActivationType.Complete);
            GameObjectPool.Instance.SaveInteractableObjectUI(_regIO.InteractableObjectUI.gameObject);
            m_interactableUIObject.Remove(_regIO);
        }
    }
    #endregion

    #region InGameplayGroup
    public void InGameplayGroup_SetActive(bool _value)
    {
        inGameplayGroup.SetActive(_value);
    }

    public void SetBallColor()
    {
        if (interactionData.Interactable && interactionData.Interactable.isInteractable)
            ballImage.color = colorOnInteraction;
        else
            ballImage.color = Color.white;
    }
    #endregion

    #endregion
}

#region Auxiliar Classes
public class RegisteredInteractableObject
{
    private InteractableBase m_interactableObject;
    private UI_InteractableObject m_interactableObjectUI;

    public RegisteredInteractableObject(InteractableBase _interactableObject)
    {
        m_interactableObject = _interactableObject;
        m_interactableObjectUI = GameObjectPool.Instance.GetInteractableObjectUI.GetComponent<UI_InteractableObject>();
    }

    #region Properties
    public InteractableBase InteractableObject
    {
        get => m_interactableObject;
        set => m_interactableObject = value;
    }

    public UI_InteractableObject InteractableObjectUI
    {
        get => m_interactableObjectUI;
        set => m_interactableObjectUI = value;
    }
    #endregion
}
#endregion